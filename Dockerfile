FROM debian: stretch-slim

# сначала добавьте нашего пользователя и группу, чтобы убедиться, что их идентификаторы получаются последовательно, независимо от того, какие зависимости добавляются
RUN groupadd -r mysql && useradd -r -g mysql mysql

RUN apt-get update && apt-get install -y -no-install-рекомендует gnupg dirmngr && rm -rf / var / lib / apt / lists / *

# добавить gosu для легкого уклонения от root
ENV GOSU_VERSION 1.7
RUN set -x \
	&& apt-get update && apt-get install -y -no-install-рекомендует ca-сертификаты wget && rm -rf / var / lib / apt / lists / * \
	&& wget -O / usr / local / bin / gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg -print-architecture)" \
	&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture) .asc" \
	&& export GNUPGHOME = "$ (mktemp -d)" \
	&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
	&& gpg --batch --verify /usr/local/bin/gosu.asc / usr / local / bin / gosu \
	&& rm -rf "$ GNUPGHOME" /usr/local/bin/gosu.asc \
	&& chmod + x / usr / local / bin / gosu \
	&& gosu nobody true \
	&& apt-get purge -y -auto-remove ca-certificate wget

RUN mkdir /docker-entrypoint-initdb.d

RUN apt-get update && apt-get install -y -no-install-рекомендует \
# для MYSQL_RANDOM_ROOT_PASSWORD
		pwgen \
# FATAL ERROR: перед выполнением / usr / local / mysql / scripts / mysql_install_db установите следующие модули Perl:
# Файл :: Basename
# Файл :: Копировать
# Sys :: Имя хоста
# Данные :: Самосвал
		perl \
# mysqld: ошибка при загрузке разделяемых библиотек: libaio.so.1: невозможно открыть файл общих объектов: нет такого файла или каталога
		libaio1 \
# mysql: ошибка при загрузке разделяемых библиотек: libncurses.so.5: невозможно открыть файл общих объектов: нет такого файла или каталога
		libncurses5 \
	&& rm -rf / var / lib / apt / lists / *

ENV MYSQL_MAJOR 5.5
ENV MYSQL_VERSION 5.5.60

RUN apt-get update && apt-get install -y ca-certificates wget -no-install-рекомендует && rm -rf / var / lib / apt / lists / * \
	&& wget "https://cdn.mysql.com/Downloads/MySQL-$MYSQL_MAJOR/mysql-$MYSQL_VERSION-linux-glibc2.12-x86_64.tar.gz" -O mysql.tar.gz \
	&& wget "https://cdn.mysql.com/Downloads/MySQL-$MYSQL_MAJOR/mysql-$MYSQL_VERSION-linux-glibc2.12-x86_64.tar.gz.asc" -O mysql.tar.gz.asc \
	&& apt-get purge -y -auto-remove ca-certificate wget \
	&& export GNUPGHOME = "$ (mktemp -d)" \
# gpg: ключ 5072E1F5: открытый ключ «MySQL Release Engineering <mysql-build@oss.oracle.com>» импортирован
	&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys A4A9406876FCBD3C456770C88C718D3B5072E1F5 \
	&& gpg --batch -verify mysql.tar.gz.asc mysql.tar.gz \
	&& rm -rf "$ GNUPGHOME" mysql.tar.gz.asc \
	&& mkdir / usr / local / mysql \
	&& tar -xzf mysql.tar.gz -C / usr / local / mysql -strip-components = 1 \
	&& rm mysql.tar.gz \
	&& rm -rf / usr / local / mysql / mysql-test / usr / local / mysql / sql-bench \
	&& rm -rf / usr / local / mysql / bin / * - debug / usr / local / mysql / bin / * _ embedded \
	&& find / usr / local / mysql -type f -name "* .a" -delete \
	&& apt-get update && apt-get install -y binutils && rm -rf / var / lib / apt / lists / * \
	&& {find / usr / local / mysql -type f -executable -exec strip -strip-all '{}' + || правда; } \
	&& apt-get purge -y -auto-remove binutils
ENV PATH $ PATH: / usr / local / mysql / bin: / usr / local / mysql / scripts

# реплицировать некоторые из способов работы конфигурации пакета APT
# это только для 5.5, поскольку у него нет ретрансляции APT, и он исчезнет, ​​когда 5.5 сделает
RUN mkdir -p /etc/mysql/conf.d \
	&& {\
		echo '[mysqld]' ; \
		echo 'skip-host-cache' ; \
		echo 'skip-name-resolve' ; \
		echo 'datadir = / var / lib / mysql' ; \
		echo '! includedir /etc/mysql/conf.d/' ; \
	}> /etc/mysql/my.cnf

RUN mkdir -p / var / lib / mysql / var / run / mysqld \
	&& chown -R mysql: mysql / var / lib / mysql / var / run / mysqld \
# убедитесь, что / var / run / mysqld (используется для файлов сокетов и блокировок) доступен для записи независимо от UID, который наш экземпляр mysqld заканчивается во время выполнения
	&& chmod 777 / var / run / mysqld

VOLUME / var / lib / mysql

COPY docker-entrypoint.sh / usr / local / bin /
RUN ln -s usr / local / bin / docker-entrypoint.sh /entrypoint.sh # обратная совместимость
ENTRYPOINT [ "docker -entrypoint.sh" ]

ЭКСПОЗИЦИЯ 3306
CMD [ "mysqld" ]